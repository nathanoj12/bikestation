import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.clustering.{KMeans, KMeansModel}
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.{DataFrame, Dataset, Row, SaveMode, SparkSession}
import org.apache.spark.sql.types._

object BikeStation{

  final var COLUMNS_FEATURES = Array("longitude","latitude")
  final var FEATURE = "feature"

  case class BikeStation(address: String, latitude: Double, longitude: Double, name: String, number: BigInt) extends Serializable

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    // Initialize Spark Session
    val spark: SparkSession = SparkSession
      .builder()
      .master("local[*]")
      .appName("BikeStationKMeans")
      .getOrCreate()

    // Allow to do some conversions
    import spark.implicits._

    // Implement Schema of dataset
    val schema = StructType(Array(
      StructField("address", StringType, true),
      StructField("latitude", DoubleType, true),
      StructField("longitude", DoubleType, true),
      StructField("name", StringType, true),
      StructField("number", IntegerType, true)
    ))

    val ds_BikeStation = getDSBikeStationFromFile(spark, "data/Brisbane_CityBike.json", schema)

    val df_KMeans = convertToFeature(ds_BikeStation,COLUMNS_FEATURES,FEATURE)

    // Split Data in training and testData. Find model to group by 5 here
    val Array(trainingData, testData) = df_KMeans.randomSplit(Array(0.7, 0.3))
    val model = modelKMeans(trainingData, FEATURE, 5, 10)

    val result = storeResultsClusteringFromDataSet(model, ds_BikeStation, "output")

    val pathModel = storeModel(model, "/output/save/model")


  }

  /** Read Json File with a given SparkSession , file path and schema
   *
   *  @param spark starts Spark with SparkSession
   *  @param fileToRead location of file to read
   *  @param schema Schema of fileToRead
   *  @return a DataSet[BikeStation] instance
   */
  def getDSBikeStationFromFile(spark: SparkSession, fileToRead: String, schema: StructType ) = {
    import spark.implicits._
    spark.read.option("inferSchema","false").schema(schema).format("json").load(fileToRead).as[BikeStation]
  }

  /** Select columns which will be our feature to implement Model
   *
   *  @param ds DataSet source
   *  @param featureColumns Array of columns chosen
   *  @param features new column on which we will define our model
   *  @return a DataSet[BikeStation] instance
   */
  def convertToFeature(ds: Dataset[BikeStation],featureColumns: Array[String], features: String) : DataFrame = {
    new VectorAssembler()
      .setInputCols(featureColumns)
      .setOutputCol(features)
      .transform(ds)
  }

  /** Determine Model
   *  @param trainingData DataSet to determine the model
   *  @param features Array of columns chosen
   *  @param nbreClusters number of groups
   *  @param nbreIterations number of Iterations
   *  @return a KMeansModel instance
   */
  def modelKMeans(trainingData: Dataset[Row], features: String, nbreClusters: Int, nbreIterations: Int) = {
    new KMeans()
      .setK(nbreClusters)
      .setFeaturesCol(features)
      .setMaxIter(nbreIterations)
      .fit(trainingData)
  }

  /** Store Model in a path
   *  @param model determine the model
   *  @param pathToStore path to Store Model
   *  @return pathToStore
   */
  def storeModel(model: KMeansModel, pathToStore: String) = {
    model.write.overwrite().save(pathToStore)
    pathToStore
  }

  /** Store results of clustering
   *  @param model determine the model
   *  @param dataset on which we will apply the model
   *  @param results path to store results of Clustering
   *  @return pathToStore
   */
  def storeResultsClusteringFromDataSet(model: KMeansModel, dataset: Dataset[BikeStation], results: String) = {
    model.transform(convertToFeature(dataset,COLUMNS_FEATURES,FEATURE))
      .drop(FEATURE) //DELETE COLUMN feature
      .coalesce(1) // Write in one file
      .write.mode(SaveMode.Overwrite)
      .option("header",true)
      .csv(results)
  }


}
